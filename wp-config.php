<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'od0+EaB43bOxMJa+33bV3ddbl7ZVq0n6Ns/7JVul+dQlau9im6JcXZS9Apkz/Sz6k1lEAPxEDDT+jbwlMBY0TA==');
define('SECURE_AUTH_KEY',  'EfqgPzxOW4/RnYr3m9qeutwNKyYRh922SK/IkAk9Cigurugy4M9jvMV1p0IM9EECpJCFQKWAanlZHobfaywnHw==');
define('LOGGED_IN_KEY',    'YacRf1dCTsYihlBsUWPaFoBq84Lx0ROrMbmcv9CcaOIr+GwFjWDxX9KMTpZXwIVWpm8aj7x9KuT1n0I2qDKvFA==');
define('NONCE_KEY',        '/bg7NFOWiUCeUzhZ7I2ZObzg5I1oaWXlsaf17q841J5+AVW3uMICbq+eUSUc1/jz0n2xgn4GHbKMIReW6HZ27A==');
define('AUTH_SALT',        'ClGy6DYMuP+y9YeCtRn0PrLD8uOes0PS57sQvP1IgIRIE7MXnAnsHaPENFzkE2k24wS5kPDdKcs2mider1jbwg==');
define('SECURE_AUTH_SALT', 'LkJ/KFbsDOk3QAHecccTiftgMzsxuYTLOWfP2Tszit3voy+6X6wNstfQ6VwAwNIijHuONOWfIcB11v+4YhM1kw==');
define('LOGGED_IN_SALT',   'd7thMWY4mWpDJzSWNw9ifNFb4dckQB0SCyg+ABxAW1E0i2fpXvxNDoz9+xeqW2M2QtxyqrTwoeqeyyqLVNg1bQ==');
define('NONCE_SALT',       '/tQgIDrQVADq8LqA4+XI6nx7mMAyWoYYk+mwjA4jLV0JiQT1hcwz2ka40Szdlp18mLkGKg1gIZ1OoXDokg3chA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
