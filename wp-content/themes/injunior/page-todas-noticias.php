<?php
   //Template Name: Todas Noticias
    get_header();
    $paged = ( get_query_var('paged')) ? get_query_var('paged'):1;
    $args = array(
        'posts_per_page' => 6,
        'paged' => $paged,
        'post_type' => 'noticias',
        'post_status' => 'publish',
        'suppress_filters' => true,
        'orderby' => 'post_date',
        'order' => 'DESC'
    );
    // The Query
    $the_query = new WP_Query( $args );
 
    /* Restore original Post Data */
    wp_reset_postdata();
?>
<main id="all-noticias" class="container">
        <h1>Todas as Notícias</h1>
        <!-- <p>Na há notícias disponíveis!</p> -->
        <?php get_search_form()?>
        <div class="news">
           
        <?php if($the_query->have_posts()) : 
                        while($the_query->have_posts()): 
                        $the_query->the_post(); ?>
                        <div class="card">
                            <h2><?php the_title()?></h2>
                            <p><?php  echo wp_strip_all_tags(get_the_content())?></p> 
                            <a href="<?php the_permalink()?>">Continuar Lendo</a> 
                        </div>
                        <?php endwhile; 
                        else: ?>
                            <p><?php _e('Sorry, no posts matched your criteria.')?></p>
                        <?php endif;    ?>
        </div>
        <div class="paginate-links">
            <?php
                $big = 999999999; // need an unlikely integer
                echo paginate_links( array(
                   'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
                   'format' => '?paged=%#%',
                   'current' => max( 1, get_query_var('paged') ),
                   'prev_text' => __('Anterior'),
                   'next_text' => __('Proximo'),
                   'total' => $the_query->max_num_pages)
                );
                   wp_reset_postdata();
            ?>
        </div>
    </main>
    
<?php
    get_footer()
?>