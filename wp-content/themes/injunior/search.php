<?php get_header()?>
<main class="container">
<?php if(have_posts()) : while(have_posts()): the_post(); ?>
    <h2><?php the_title()?></h2>
    <a href="<?php the_permalink()?>">Link</a>
<?php endwhile;
else: ?>
    <h2>Não temos posts</h2>
<?php endif; ?>
</main>

<?php get_footer()?>