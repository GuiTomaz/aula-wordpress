<?php
    remove_action('wp_head', 'rsd_link');
    remove_action('wp_head', 'wlwmanifest_link');
    remove_action('wp_head', 'start_post_rel_link', 10, 0);
    remove_action('wp_head', 'feed_links_extra', 3);
    remove_action('wp_head', 'wp_generator');
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');
    function add_styles_and_scripts(){
        if(!is_single()){
            wp_enqueue_style("reset-sheet", get_template_directory_uri() . "/assets/css/reset.css");
        }
        
        wp_enqueue_style("style-sheet", get_template_directory_uri() . "/style.css");
        wp_enqueue_script("mail-go-script", get_template_directory_uri() . "/assets/js/mailgo.min.js");
    }
    add_action('wp_enqueue_scripts', 'add_styles_and_scripts');

    function create_custom_post_news() {
 
        // Set the labels, this variable is used in the $args array
        $labels = array(
          'name'               => __( 'Notícias' ),
          'singular_name'      => __( 'Notícia' ),
          'add_new'            => __( 'Adicionar Nova Notícia' ),
          'add_new_item'       => __( 'Adicionar Nova Notícia' ),
          'edit_item'          => __( 'Editar Notícia' ),
          'new_item'           => __( 'Nova notícia' ),
          'all_items'          => __( 'Todas as notícias' ),
          'view_item'          => __( 'Ver notícia' ),
          'search_items'       => __( 'Pesquisar notícia' ),
          'featured_image'     => 'Poster',
          'set_featured_image' => 'Add Poster'
        );
       
        // The arguments for our post type, to be entered as parameter 2 of register_post_type()
        $args = array(
          'labels'            => $labels,
          'description'       => 'Noticias do site da Aula Wordpress',
          'public'            => true,
          'menu_position'     => 5,
          'supports'          => array( 'title', 'editor', 'thumbnail', 'excerpt', 'comments', 'custom-fields' ),
          'has_archive'       => true,
          'show_in_admin_bar' => true,
          'show_in_nav_menus' => true,
          'has_archive'       => true,
          'query_var'         => 'noticias'
        );
        register_post_type( 'noticias', $args);
        unregister_post_type( 'noticia' );
        unregister_post_type('news');
        unregister_post_type( 'movie' );
    }
    add_action('init', 'create_custom_post_news');

?>