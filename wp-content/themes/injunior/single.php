<?php get_header();

    $args = array(
        'posts_per_page' => 3,
        'paged' => $paged,
        'post_type' => 'noticias',
        'post_status' => 'publish',
        'suppress_filters' => true,
        'orderby' => 'rand',
        'order' => 'DESC'
    );
    $the_query = new WP_Query($args);
?>
<main id="single-noticia" class="container">
        <div class="card">
            <div>
                <h1><?php the_title()?></h1>
                <div>
                    <p>Criado em: <?php echo get_the_date()?></p>
                    <p>Editado em: <?php echo get_the_modified_date()?></p>
                </div>
            </div>
            <?php the_content()?>
        </div>
        <div id="side-news">
            <h3>Artigos Relacionados</h3>
            <div>
                <?php if($the_query->have_posts()) : 
                            while($the_query->have_posts()): 
                            $the_query->the_post(); ?>
                            <div class="card">
                                <h5><?php the_title()?></h5>
                                <p>Criado em: <?php echo get_the_date()?> </p>
                                <p><?php  echo wp_strip_all_tags(get_the_content())?></p> 
                                <a href="<?php the_permalink()?>">Continuar Lendo</a> 
                            </div>
                            <?php endwhile; endif;?>
            </div>
        </div>
    </main>
<?php get_footer() 
?>