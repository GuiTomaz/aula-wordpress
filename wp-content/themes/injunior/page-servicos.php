<?php
    //Template Name: Servicos
    $home = get_page_by_title('Homepage');
    get_header()
?>
<main id="servicos">
        <h1>Serviços</h1>
        <section class="container">
            <div class="card">
                <div><h2><?php the_field('primeiro_servico', $home)?></h2></div>
                <ul>
                    <li>Paranauê</li>
                    <li>Paranauê</li>
                    <li>Paraná</li>
                </ul>
            </div>
            <div class="card">
                <div><h2><?php the_field('segundo_servico', $home)?></h2></div>
                <ul>
                    <li>Pipipipopopo</li>
                    <li>Pipipipopopo</li>
                    <li>Pipipipopopo</li>
                </ul>
            </div>
            <div class="card">
                <div><h2><?php the_field('terceiro_servico', $home)?></h2></div>
                <ul>
                    <li>pára pá pá</li>
                    <li>pá pá pá</li>
                    <li>POOOOOWWW</li>
                </ul>
            </div>
        </section>
    </main>
<?php
    get_footer()
?>