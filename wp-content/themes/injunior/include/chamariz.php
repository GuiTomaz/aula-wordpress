<?php
     $home = get_page_by_title('Homepage');
?>
<section id="sct-1">
            <h1><?php bloginfo('name')?></h1>
            <p><?php bloginfo('description')?></p>  
            <img src="<?php the_field('bg-grande', $home)?>" alt="">
            <!-- <?php if (is_front_page()):?>
                <h3>Homepageee</h3>
            <?php else: ?>
                <h3>Noticias</h3>  
            <?php endif;?> -->
        </section>